/*
 * Vector.h
 *
 *  Created on: 21-apr.-2015
 *      Author: niels
 */

#ifndef SRC_UTIL_VECTOR_H_
#define SRC_UTIL_VECTOR_H_
#include "Point.h"
#include <algorithm>
class Vector {
private:
	double x;
	double y;
public:

	Vector(double x, double y):x(x), y(y){}
	Vector(const Point& from, const Point& to);
	double dot(const Vector& v);
	double length();
	void normalize();
	double getY()const{return y;}
	double getX()const{return x;}
	friend Point operator+(const Vector & v, const Point & p);
	friend Point operator+(const Point & p, const Vector & v);
	friend Vector operator*(const Vector & v, const double& d);
	friend Vector operator*(const double& d, const Vector & v);
	friend Vector operator+(const Vector &v1, const Vector & v2);
	friend Vector operator-(const Vector &v1, const Vector & v2);
};



#endif /* SRC_UTIL_VECTOR_H_ */
