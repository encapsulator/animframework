/*
 * Vector.cpp
 *
 *  Created on: 21-apr.-2015
 *      Author: niels
 */

#include "Vector.h"
#include <math.h>

Vector::Vector(const Point &from, const Point &to) {
	this->x = to.x - from.x;
	this->y = to.y - from.y;
}

double Vector::dot(const Vector & v) {
	return (x*v.x)+(y*v.y);
}

double Vector::length() {
	return sqrt(pow(x,2)+pow(y,2));
}

void Vector::normalize() {
	double factor = 1/length();
	this->x = factor * x;
	this->y = factor * y;
}

Point operator+(const Point& p, const Vector& v) {
	return Point(p.x+v.x, p.y+v.y);
}

Point operator+(const Vector & v, const Point & p) {
	return p + v;
}

Vector operator*(const Vector &v, const double & d) {
	return Vector(v.x*d, v.y*d);
}

Vector operator*(const double & d, const Vector & v) {
	return v * d;
}

Vector operator+(const Vector& v1, const Vector& v2) {
	return Vector(v1.x + v2.x, v1.y + v2.y);
}

Vector operator-(const Vector& v1, const Vector& v2) {
	return Vector(v1.x - v2.x, v1.y - v2.y);
}
