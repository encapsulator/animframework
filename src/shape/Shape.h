/*
 * Shape.h
 *
 *  Created on: 21-apr.-2015
 *      Author: niels
 */

#ifndef SRC_SHAPE_SHAPE_H_
#define SRC_SHAPE_SHAPE_H_
#include "IDrawable.h"
#include "util/Colour.h"
class Shape:public IDrawable {
protected:
	Colour colour;
public:
	Shape(Colour & colour):colour(colour){}
	virtual ~Shape(){}
	Colour getColour() const{return colour;}
	void setColour(Colour & colour) {
		this->colour = colour;
	}

};

#endif /* SRC_SHAPE_SHAPE_H_ */
