/*
 * Boid.cpp
 *
 *  Created on: 12-mei-2015
 *      Author: niels
 */

#include <shape/Boid.h>
#include <cmath>
#include <windows.h>
#include <GL/glut.h>
#include <iostream>

void Boid::update(const Scene & scene) {
	this->setCentre(centre + velocity);
}

void Boid::draw() const {
	glColor3f(colour.r, colour.g, colour.b);
	glBegin(GL_POLYGON);

	//Calculate angle between u and v
	Vector u(1, 0);
	double vdotu = u.dot(velocity);
	double lenv = getVelocity().length();
	double radians = vdotu / lenv;
	double angleP = acos(radians);
	//Calculate point P
	float px = centre.x + size * cos((angleP * M_PI)/180);
	float py = centre.y + size * sin((angleP * M_PI)/180);
	cout << px << "  " << py << endl;
	glVertex2f(px, py);
	angleP += 150;
	//Calculate Point q
	float qx = centre.x + size * cos((angleP * M_PI)/180);
	float qy = centre.y + size * sin((angleP * M_PI)/180);cout << qx << "  " << qy << endl;
	glVertex2f(qx, qy);
	angleP += 60;

	//Calculate point R

	float rx = centre.x + size * cos((angleP * M_PI)/180);
	float ry = centre.y + size * sin((angleP * M_PI)/180);cout << rx << "  " << ry << endl;
	glVertex2f(rx, ry);

	glEnd();
}
