/*
 * Boid.h
 *
 *  Created on: 12-mei-2015
 *      Author: niels
 */

#ifndef SRC_SHAPE_BOID_H_
#define SRC_SHAPE_BOID_H_

#include "Shape.h"
#include "IAnimatable.h"
#include "util/Point.h"
#include "util/Vector.h"
#include "util/Colour.h"
#include "scene/Scene.h"
class DynamicCircle;
class Boid: public Shape, public IAnimatable {
	private:
		Point centre;
		double size;
		Vector velocity;

	public:
		Boid(Colour c, Point centre, double size, Vector velocity):Shape(c), centre(centre), size(size), velocity(velocity) {}
		Point getCentre() const { return centre;}
		void setCentre(Point centre) { this->centre = centre;}
		double getSize() const {return size;}
		void setSize(double size) {this->size = size;}
		Vector getVelocity() const {return velocity;}
		void setVelocity(Vector velocity){this->velocity = velocity;}
		void intersection(DynamicCircle * dynamicCircle) const {}
		void update(const Scene & scene);
		virtual void draw() const;
};

#endif /* SRC_SHAPE_BOID_H_ */
