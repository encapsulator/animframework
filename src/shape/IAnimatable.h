/*
 * IAnimatable.h
 *
 *  Created on: 21-apr.-2015
 *      Author: niels
 */

#ifndef SRC_SHAPE_IANIMATABLE_H_
#define SRC_SHAPE_IANIMATABLE_H_

class Scene;

class IAnimatable {
public:
	virtual void update(const Scene & scene) =0;
	virtual ~IAnimatable() {}

};



#endif /* SRC_SHAPE_IANIMATABLE_H_ */
