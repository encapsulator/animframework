/*
 * IDrawable.h
 *
 *  Created on: 21-apr.-2015
 *      Author: niels
 */

#ifndef SRC_SHAPE_IDRAWABLE_H_
#define SRC_SHAPE_IDRAWABLE_H_
class DynamicCircle;
class IDrawable {
public:
	virtual void draw() const =0;
	virtual ~IDrawable(){}
	virtual void intersection(DynamicCircle * dynamicCircle) const = 0;
};

#endif /* SRC_SHAPE_IDRAWABLE_H_ */
