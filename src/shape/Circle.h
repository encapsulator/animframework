/*
 * Circle.h
 *
 *  Created on: 21-apr.-2015
 *      Author: niels
 */

#ifndef SRC_SHAPE_CIRCLE_H_
#define SRC_SHAPE_CIRCLE_H_
#include "util/Point.h"
#include "util/Colour.h"
#include <windows.h>
#include <GL/glut.h>
#include "Shape.h"
class DynamicCircle;

class Circle: public Shape {
protected:
	Point center;
	int radius;
public:
	Circle(Point center, int radius, Colour c):Shape(c), center(center), radius(radius){}
	virtual ~Circle(){}
	Point getCenter() const {return center;}
	void setCenter(Point p) {
		this->center = p;
	}
	int getRadius() const {return radius;}
	void setRadius(int radius) {
		this->radius = radius;
	}
	virtual void draw() const;
	void intersection(DynamicCircle * dynamicCircle) const;
};

#endif /* SRC_SHAPE_CIRCLE_H_ */
