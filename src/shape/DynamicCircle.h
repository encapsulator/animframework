/*
 * DynamicCircle.h
 *
 *  Created on: 21-apr.-2015
 *      Author: niels
 */

#ifndef SRC_SHAPE_DYNAMICCIRCLE_H_
#define SRC_SHAPE_DYNAMICCIRCLE_H_
#include "util/Vector.h"
#include "util/Colour.h"
#include "util/Point.h"
#include "Circle.h"
#include "IAnimatable.h"
class DynamicCircle: public Circle, public IAnimatable {
private:
	Vector velocity;
public:
	DynamicCircle(Point point, int radius, Colour colour, Vector v):Circle(point, radius, colour), velocity(v) {}
	virtual ~DynamicCircle(){}
	Vector getVelocity() const {return velocity;}
	void setVelocity(Vector velocity) {this->velocity = velocity;}
	void update(const Scene & scene);
	void intersection(DynamicCircle * dynamicCircle) const {}
};

#endif /* SRC_SHAPE_DYNAMICCIRCLE_H_ */
