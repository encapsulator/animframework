/*
 * Circle.cpp
 *
 *  Created on: 21-apr.-2015
 *      Author: niels
 */

#include "Circle.h"
#include <math.h>
#include "DynamicCircle.h"

void Circle::draw() const {
	glColor3f(colour.r, colour.g, colour.b);
	glBegin(GL_POLYGON);

	for (double i = 0; i <= 360; i += 7.2) {
		float x = center.x + radius * cos((i * M_PI) / 180);
		float y = center.y + radius * sin((i * M_PI) / 180);
		glVertex2f(x, y);
	}

	glEnd();
}

void Circle::intersection(DynamicCircle * dynamicCircle) const {
	if (center.distance(dynamicCircle->center)
			< radius + dynamicCircle->radius) {
		Vector u1(dynamicCircle->center.x - center.x,
				dynamicCircle->center.y - center.y);
		Vector velocity = dynamicCircle->getVelocity();

		//Set correct position
		double a = velocity.dot(velocity);
		double b = 2 * (velocity.dot(u1));
		double c = u1.dot(u1) - (pow(dynamicCircle->radius + radius, 2));

		double discriminant = (b * b - (4 * a * c));
		double x1 = (-b + sqrt(discriminant)) / (2 * a);
		double x2 = (-b - sqrt(discriminant)) / (2 * a);

		double t = x1 <= x2 ? x1 : x2;
		dynamicCircle->setCenter(dynamicCircle->center + t * velocity);

		//Update velocity
		Vector u2(dynamicCircle->center.x - center.x,
				dynamicCircle->center.y - center.y);
		u2.normalize();
		dynamicCircle->setVelocity(velocity - ((2 * velocity.dot(u2)) * u2));

	}
}
