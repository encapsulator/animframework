/*
 * DynamicCircle.cpp
 *
 *  Created on: 21-apr.-2015
 *      Author: niels
 */

#include <shape/DynamicCircle.h>
#include <vector>
#include "scene/Scene.h"

void DynamicCircle::update(const Scene & scene) {
	this->setCenter(center + velocity);
	std::vector<const IDrawable*> drawables = scene.getDrawables();

	for (vector<const IDrawable*>::iterator iter = drawables.begin(); iter != drawables.end(); ++iter) {
		(*iter)->intersection(this);
	}

}
