/*
 * Scene.cpp
 *
 *  Created on: 28-apr.-2015
 *      Author: niels
 */


#include "scene/Scene.h"
#include "scene/WorldWindow.h"
#include <iostream>

Scene::Scene(const WorldWindow * worldWindow) {
	drawables.push_back(worldWindow);
}

Scene::~Scene() {
	for (vector<const IDrawable*>::iterator iter = drawables.begin(); iter != drawables.end(); ++iter) {
		delete *iter;
	}
}

const vector<const IDrawable*> Scene::getDrawables() const {
	return drawables;
}

const vector<IAnimatable*> Scene::getAnimatables() const {
	return animatables;
}

void Scene::addAnimatable(IAnimatable *animatable) {
	animatables.push_back(animatable);
}

void Scene::addDrawable(IDrawable *drawable) {
	drawables.push_back(drawable);
}

