/*
 * Scene.h
 *
 *  Created on: 28-apr.-2015
 *      Author: niels
 */

#ifndef SRC_SCENE_SCENE_H_
#define SRC_SCENE_SCENE_H_
#include "shape/IDrawable.h"
#include "shape/IAnimatable.h"
#include <vector>
class WorldWindow;
using namespace std;
class Scene {
private:
	vector<const IDrawable*>  drawables;
	vector<IAnimatable*> animatables;

public:
	Scene(const WorldWindow * worldWindow);
	~Scene();
	const vector<const IDrawable*> getDrawables() const;
	const vector<IAnimatable*> getAnimatables() const;
	void addDrawable(IDrawable * drawable);
	void addAnimatable(IAnimatable * animatable);

};



#endif /* SRC_SCENE_SCENE_H_ */
