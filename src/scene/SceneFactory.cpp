/*
 * SceneFactory.cpp
 *
 *  Created on: 28-apr.-2015
 *      Author: niels
 */
#include "scene/Scene.h"
#include "scene/SceneFactory.h"
#include <fstream>
#include "util/Colour.h"
#include "util/Point.h"
#include "shape/Circle.h"
#include "shape/DynamicCircle.h"
#include "util/Vector.h"
#include "shape/Boid.h"
#include <iostream>

Scene *SceneFactory::createScene(const char * filename,
		const WorldWindow * worldWindow) {
	ifstream inf(filename);
	if (!inf) {
		exit(1);
	}
	Colour colour(0, 0, 0);
	Scene *scene = new Scene(worldWindow);
	while (inf) {
		string keyword;
		inf >> keyword;

		if (keyword.compare("colour") == 0) {
			int r;
			int g;
			int b;
			inf >> r;
			inf >> g;
			inf >> b;
			colour = Colour(r, g, b);
		}
		if (keyword.compare("circle") == 0
				|| keyword.compare("dynamicCircle") == 0
				|| keyword.compare("boid") == 0) {
			double x;
			double y;
			double radius;
			inf >> x;
			inf >> y;
			inf >> radius;

			Point p(x, y);
			if (keyword.compare("circle") == 0) {
				Circle *circle = new Circle(p, radius, colour);
				scene->addDrawable(circle);
			} else {
				double vx;
				double vy;
				inf >> vx;
				inf >> vy;
				Vector v(vx, vy);

				if (keyword.compare("dynamicCircle") == 0) {
					DynamicCircle *circle = new DynamicCircle(p, radius, colour,
							v);
					scene->addDrawable(circle);
					scene->addAnimatable(circle);
				}

				if (keyword.compare("boid") == 0) {
					Boid * boid = new Boid(colour, p, radius, v);
					scene->addDrawable(boid);
					scene->addAnimatable(boid);
				}
			}
		}
	}
	return scene;
}

