/*
 * WorldWindow.cpp
 *
 *  Created on: 6-mei-2015
 *      Author: niels
 */

#include "WorldWindow.h"

void WorldWindow::intersection(DynamicCircle * dynamicCircle) const {
	bool bounced = false;
	Point center = dynamicCircle->getCenter();
	int radius = dynamicCircle->getRadius();
	Vector velocity = dynamicCircle->getVelocity();
	Vector m (0,0);
		if(center.x+radius >= right && velocity.getX()>0) {
			m = Vector(-1, 0);
			bounced = true;
		}
		if(center.x-radius <= left && velocity.getX()<0) {
			m = Vector (1, 0);
			bounced = true;
		}

		if(center.y-radius <= bottom && velocity.getY()<0) {
			m = Vector (0, 1);
			bounced = true;
		}

		if(center.y + radius >= top && velocity.getY()>0) {
			m = Vector(0, -1);
			bounced = true;
		}

		if(bounced) {
			dynamicCircle->setVelocity(velocity - (2 * velocity.dot(m)) * m);
		}

		dynamicCircle->setCenter(center + velocity);
}


