/*
 * SceneFactory.h
 *
 *  Created on: 28-apr.-2015
 *      Author: niels
 */

#ifndef SRC_SCENE_SCENEFACTORY_H_
#define SRC_SCENE_SCENEFACTORY_H_
#include "scene/Scene.h"
#include <string>
#include "WorldWindow.h"
class SceneFactory {
public:
	static Scene *createScene(const char* filename, const WorldWindow * worldWindow);
};


#endif /* SRC_SCENE_SCENEFACTORY_H_ */
