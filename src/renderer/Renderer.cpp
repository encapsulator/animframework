#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <windows.h>
#include <GL/glut.h>
#endif

#include "renderer/Renderer.h"
#include "shape/Circle.h"
#include "util/Point.h"
#include "util/Colour.h"
#include <vector>
#include <iostream>

void Renderer::render(){

	vector<const IDrawable*> drawables = scene->getDrawables();
	vector<IAnimatable*> animatables = scene->getAnimatables();


	for (vector<const IDrawable*>::iterator iter = drawables.begin(); iter != drawables.end(); ++iter) {
		(*iter)->draw();
	}

	for (vector<IAnimatable*>::iterator iter = animatables.begin(); iter != animatables.end(); ++iter) {
		(*iter)->update(*scene);
	}

}


