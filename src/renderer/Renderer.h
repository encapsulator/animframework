#ifndef RENDERER_H_
#define RENDERER_H_
#include "shape/DynamicCircle.h"
#include "scene/Scene.h"
class Renderer {
private:
	Scene *scene;

public:
	Renderer(Scene *scene){
		this->scene = scene;
	}
	virtual ~Renderer() {}
	void render();
};

#endif /* RENDERER_H_ */

